
// File I/O
// Austin Lennert

#include <iostream>
#include <conio.h>
#include <string>
#include <fstream>  // "File" Stream

using namespace std;

void printSomething(string text, ostream& outStream) {  // 'output' stream
	outStream << text;
}

int main()
{


	string filePath = "C:\\Users\\aulen\\AppData\\Roaming\\DEBUG\\test.txt";

	/**/
	// Write to file
	ofstream ofs(filePath);  // output file stream
	ofs << "Lennert\nHowdy";
	ofs << "\n";
	ofs.close();
	/**/

	/**
	string name;
	cout << "Enter your name: ";
	//cin >> name;  // Goes until any whitespace is found
	getline(cin, name);  // Goes until a '\n' newline is found
	cout << "your name is " << name;
	/**/


	// Read from file
	string line;
	ifstream ifs(filePath);  // input file stream
	while (getline(ifs, line)) {  // Returns 'null' or '0' when there is no new line; ie end of file
		cout << line << '\n';
	}



	cout << "\n\n\nPress any key to exit...";
	(void)_getch();
	return 0;
}
